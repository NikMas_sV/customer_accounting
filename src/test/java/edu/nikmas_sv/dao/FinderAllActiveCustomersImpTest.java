package edu.nikmas_sv.dao;

import edu.nikmas_sv.domain.Customer;
import edu.nikmas_sv.exception.DaoException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.Assert.*;

public class FinderAllActiveCustomersImpTest {

    private static final Logger logger = LoggerFactory.getLogger(FinderCustomerBySurnameDaoImpTest.class);

    @BeforeClass
    public static void startUp() throws Exception {
        DBInit.startUp();
    }
    @Test
    public void
    testCustomers() throws DaoException {
        List<Customer> cst = new FinderAllActiveCustomersImp().findAllActiveCustomers("ПРИОСТАНОВЛЕН");
        Assert.assertTrue(cst.size() == 2);
    }
}