package edu.nikmas_sv.dao;

import edu.nikmas_sv.domain.Customer;
import edu.nikmas_sv.exception.DaoException;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class AdditionCustomerDaoImpTest {
    @BeforeClass
    public static void startUp() throws Exception {
        URL url1 = FinderCustomerBySurnameDaoImpTest.class.getClassLoader()
                .getResource("customer_accounting.sql");
        URL url2 = FinderCustomerBySurnameDaoImpTest.class.getClassLoader()
                .getResource("customer_accounting_data.sql");

        List<String> str1 = Files.readAllLines(Paths.get(url1.toURI()));
        String sql1 = str1.stream().collect(Collectors.joining());

        List<String> str2 = Files.readAllLines(Paths.get(url2.toURI()));
        String sql2 = str2.stream().collect(Collectors.joining());

        try (Connection con = ConnectionBuilder.getConnection();
             Statement stmt = con.createStatement();
        ) {
            stmt.executeUpdate(sql1);
            stmt.executeUpdate(sql2);
        }
    }

    public static Customer buildCustomer() {
        Customer cst = new Customer();
        cst.setSurName("Pupkin");
        cst.setGivenName("Vasya");
        cst.setPatronymic("Pupkovich");
        cst.setDateOfBirth(LocalDate.of(2000, 9, 15));
        cst.setTelNumber("+375333091936");
        cst.setGender("M");
        cst.setSourceAdvertising("VK");
        cst.setIdOfSeasonTicket("ULT1");
        return cst;
    }

//    @Ignore
    @Test
    public void saveAdditionCustomer() throws DaoException {
        Customer cst = buildCustomer();
        Long id = new AdditionCustomerDaoImp().saveAdditionCustomer(cst);
    }

    @Test(expected = DaoException.class)
    public void saveAdditionCustomerError() throws DaoException {
        Customer cst = buildCustomer();
        cst.setSurName(null);
        Long id = new AdditionCustomerDaoImp().saveAdditionCustomer(cst);

    }
}