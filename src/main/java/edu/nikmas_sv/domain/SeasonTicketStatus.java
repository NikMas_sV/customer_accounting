package edu.nikmas_sv.domain;

public enum SeasonTicketStatus {
    АКТИВЕН, ПРИОСТАНОВЛЕН, ОКОНЧЕН;

    public static SeasonTicketStatus getValue(int value) {
        for (SeasonTicketStatus sts : SeasonTicketStatus.values()) {
            if (sts.ordinal() == value) {
                return sts;
            }
        }
        throw new RuntimeException("Неизвестный статус абонемента: " + value);
    }

    public static int getNumber(String value) {
        for (SeasonTicketStatus sts : SeasonTicketStatus.values()) {
            if (value.equals(sts.toString())) {
                return sts.ordinal();
            }
        }
        throw new RuntimeException("Введен неизвестный статус абонемента: " + value);
    }
}

