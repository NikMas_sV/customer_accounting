package edu.nikmas_sv.domain;

public class Ticket {

    private String tIdOfSeasonTicket;
    private String feeTicket;

    public String gettIdOfSeasonTicket() {
        return tIdOfSeasonTicket;
    }

    public void settIdOfSeasonTicket(String tIdOfSeasonTicket) {
        this.tIdOfSeasonTicket = tIdOfSeasonTicket;
    }

    public String getFeeTicket() {
        return feeTicket;
    }

    public void setFeeTicket(String feeTicket) {
        this.feeTicket = feeTicket;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "tIdOfSeasonTicket='" + tIdOfSeasonTicket + '\'' +
                ", feeTicket='" + feeTicket + '\'' +
                '}';
    }
}
