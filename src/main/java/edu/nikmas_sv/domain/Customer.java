package edu.nikmas_sv.domain;

import java.time.LocalDate;

public class Customer {

    private long idAccount;
    private String surName;
    private String givenName;
    private String patronymic;
    private LocalDate dateOfBirth;
    private String telNumber;
    private String gender;
    private String sourceAdvertising;
    private String idOfSeasonTicket;
    private SeasonTicketStatus seasonTicketStatus;
    private LocalDate dateOfPurchase;

    public Customer() {
    }

    public Customer(long idAccount, String surName, String givenName, String patronymic, LocalDate dateOfBirth, String telNumber, String gender, String sourceAdvertising, String idOfSeasonTicket, SeasonTicketStatus seasonTicketStatus, LocalDate dateOfPurchase) {
        this.idAccount = idAccount;
        this.surName = surName;
        this.givenName = givenName;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.telNumber = telNumber;
        this.gender = gender;
        this.sourceAdvertising = sourceAdvertising;
        this.idOfSeasonTicket = idOfSeasonTicket;
        this.seasonTicketStatus = seasonTicketStatus;
        this.dateOfPurchase = dateOfPurchase;
    }

    public long getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(long idAccount) {
        this.idAccount = idAccount;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSourceAdvertising() {
        return sourceAdvertising;
    }

    public void setSourceAdvertising(String sourceAdvertising) {
        this.sourceAdvertising = sourceAdvertising;
    }

    public String getIdOfSeasonTicket() {
        return idOfSeasonTicket;
    }

    public void setIdOfSeasonTicket(String idOfSeasonTicket) {
        this.idOfSeasonTicket = idOfSeasonTicket;
    }

    public SeasonTicketStatus getSeasonTicketStatus() {
        return seasonTicketStatus;
    }

    public void setSeasonTicketStatus(SeasonTicketStatus seasonTicketStatus) {
        this.seasonTicketStatus = seasonTicketStatus;
    }

    public LocalDate getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(LocalDate dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "idAccount=" + idAccount +
                ", surName='" + surName + '\'' +
                ", givenName='" + givenName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", telNumber='" + telNumber + '\'' +
                ", gender='" + gender + '\'' +
                ", sourceAdvertising='" + sourceAdvertising + '\'' +
                ", idOfSeasonTicket='" + idOfSeasonTicket + '\'' +
                ", seasonTicketStatus=" + seasonTicketStatus +
                ", dateOfPurchase=" + dateOfPurchase +
                '}';
    }
}
