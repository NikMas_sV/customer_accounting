package edu.nikmas_sv.dao;

import edu.nikmas_sv.domain.Customer;
import edu.nikmas_sv.domain.SeasonTicketStatus;
import edu.nikmas_sv.exception.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class FinderAllActiveCustomersImp implements FinderAllActiveCustomers {

    public static final Logger logger = LoggerFactory.getLogger(FinderCustomerBySurnameDaoImp.class);

    public static final String GET_ALL_ACTIVE_CUSTOMERS = "SELECT id_account, surname, givenname, patronymic, date_of_birth, " +
            "tel_number, gender, source_advertising, id_of_season_ticket, status, date_of_purchase " +
            "FROM ca_customers WHERE UPPER(status) LIKE UPPER(?);";

    private Connection getConnection() throws SQLException {
        return ConnectionBuilder.getConnection();
    }

    @Override
    public List<Customer> findAllActiveCustomers(String pattern) throws DaoException {
        List<Customer> result = new LinkedList<>();

        try (Connection con = getConnection();
             PreparedStatement stmt = con.prepareStatement(GET_ALL_ACTIVE_CUSTOMERS)) {
            stmt.setString(1, "%" + pattern + "%");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Customer cst = new Customer(rs.getLong("id_account"),
                        rs.getString("surname"), rs.getString("givenname"),
                        rs.getString("patronymic"), rs.getDate("date_of_birth").toLocalDate(),
                        rs.getString("tel_number"), rs.getString("gender"),
                        rs.getString("source_advertising"), rs.getString("id_of_season_ticket"),
                        //TODO подумать что тут лучше сделать
                        SeasonTicketStatus.getValue(SeasonTicketStatus.getNumber(rs.getString("status"))),
                        rs.getDate("date_of_purchase").toLocalDate());
                result.add(cst);
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex);
        }
        return result;
    }


}
