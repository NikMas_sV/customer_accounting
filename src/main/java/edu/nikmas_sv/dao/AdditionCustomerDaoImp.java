package edu.nikmas_sv.dao;

import edu.nikmas_sv.domain.Customer;
import edu.nikmas_sv.domain.SeasonTicketStatus;
import edu.nikmas_sv.exception.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;


public class AdditionCustomerDaoImp implements AdditionCustomerDao {

    public static final Logger logger = LoggerFactory.getLogger(FinderCustomerBySurnameDaoImp.class);

    public static final String INSERT_CUSTOMER =
            "INSERT INTO public.ca_customers(" +
                    "surname, givenname, patronymic, " +
                    "date_of_birth, tel_number, gender, source_advertising," +
                    " id_of_season_ticket, status, date_of_purchase)" +
                    "VALUES (?, ?, ?, " +
                    "?, ?, ?, ?, " +
                    "?, ?, ?);";

    private Connection getConnection() throws SQLException {
        return ConnectionBuilder.getConnection();
    }

    public Long saveAdditionCustomer(Customer cst) throws DaoException {
        Long result = -1L;

        try (Connection con = getConnection();
             PreparedStatement stmt = con.prepareStatement(INSERT_CUSTOMER, new String[]{"id_account"})) {

            con.setAutoCommit(false);
            try {
                stmt.setString(1, cst.getSurName());
                stmt.setString(2, cst.getGivenName());
                stmt.setString(3, cst.getPatronymic());
                stmt.setDate(4, java.sql.Date.valueOf(cst.getDateOfBirth()));
                stmt.setString(5, cst.getTelNumber());
                stmt.setString(6, cst.getGender());
                stmt.setString(7, cst.getSourceAdvertising());
                stmt.setString(8, cst.getIdOfSeasonTicket());
                stmt.setString(9, String.valueOf(SeasonTicketStatus.АКТИВЕН));
                stmt.setDate(10, java.sql.Date.valueOf(LocalDate.now()));

                stmt.executeUpdate();

                ResultSet gkRs = stmt.getGeneratedKeys();
                if (gkRs.next()) {
                    result = gkRs.getLong(1);
                }
                gkRs.close();

                con.commit();
            } catch (SQLException ex) {
                con.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex);
        }
        return 1L;
    }
}
