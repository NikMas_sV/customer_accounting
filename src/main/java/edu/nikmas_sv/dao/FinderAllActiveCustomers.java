package edu.nikmas_sv.dao;

import edu.nikmas_sv.domain.Customer;
import edu.nikmas_sv.exception.DaoException;

import java.util.List;

public interface FinderAllActiveCustomers {
    List<Customer> findAllActiveCustomers(String pattern) throws DaoException;
}
