package edu.nikmas_sv.dao;

import edu.nikmas_sv.domain.Customer;
import edu.nikmas_sv.exception.DaoException;

public interface AdditionCustomerDao {
    Long saveAdditionCustomer(Customer cst) throws DaoException;
}
