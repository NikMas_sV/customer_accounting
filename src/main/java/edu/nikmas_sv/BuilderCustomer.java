package edu.nikmas_sv;

import edu.nikmas_sv.domain.Customer;

import java.time.LocalDate;

public class BuilderCustomer {
    public static Customer buildCustomer() {
        Customer cst = new Customer();
        cst.setSurName("Pupkin");
        cst.setGivenName("Vasya");
        cst.setPatronymic("Pupkovich");
        cst.setDateOfBirth(LocalDate.of(2000, 9, 15));
        cst.setTelNumber("+375333091936");
        cst.setGender("M");
        cst.setSourceAdvertising("VK");
        cst.setIdOfSeasonTicket("ULT1");
        return cst;
    }
}