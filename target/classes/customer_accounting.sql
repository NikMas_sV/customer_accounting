DROP TABLE IF EXISTS ca_customers;
DROP TABLE IF EXISTS types_of_ticket;

  CREATE TABLE types_of_ticket
  (
  	t_id_of_season_ticket varchar(10) NOT NULL,
  	fee_ticket double precision,
  	PRIMARY KEY (t_id_of_season_ticket)
  );

  CREATE TABLE ca_customers
  (
  	id_account SERIAL,
    surname varchar(15) NOT NULL,
    givenname varchar(15) NOT NULL,
    patronymic varchar(15) NOT NULL,
    date_of_birth date,
    tel_number varchar(15),
    gender varchar(3) NOT NULL,
    source_advertising varchar(15),
    id_of_season_ticket varchar(10) NOT NULL,
  	status varchar (15) NOT NULL,
  	date_of_purchase date NOT NULL,
  	PRIMARY KEY (id_account),
  	FOREIGN KEY (id_of_season_ticket) REFERENCES types_of_ticket(t_id_of_season_ticket) ON DELETE RESTRICT
  );
